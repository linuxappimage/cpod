# CPod AppImage

[CPod](https://github.com/z-------------/CPod) A simple, beautiful podcast app.

You can download the **[CPod.AppImage](https://gitlab.com/linuxappimage/cpod/-/jobs/artifacts/master/raw/CPod.AppImage?job=run-build)** binary.


Notes for you Linux shell:

```bash
curl -sLO CPod.AppImage https://gitlab.com/linuxappimage/cpod/-/jobs/artifacts/master/raw/CPod.AppImage?job=run-build

chmod +x CPod.AppImage
./CPod.AppImage
```
